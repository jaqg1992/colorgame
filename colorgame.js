
//Random number generator from 0 - 255
function randomNum(){
  return Math.floor(Math.random() * 255);
}
//Random number generator from 0 - 6
function randNum(input){
    return Math.floor(Math.random() * input);
}

var colors = new Array(); //Colors array declared
var numSquares = 6;
fillArray(numSquares);
var squares = document.querySelectorAll(".square");
var colorDisplay = document.getElementById("colorPicked");
var winner = document.getElementById("win");
var banner = document.querySelector("#fbanner");
var newCols = document.getElementById("newCols");
var colorChose = colorChosen(numSquares);
var modeButtons = document.querySelectorAll(".mode");

init();

function init(){
  //Mode buttons event listeners
  initModeButtons();
  initSquares()
  reset();
}

function initModeButtons(){
  for(var i = 0; i < modeButtons.length; i++){
    modeButtons[i].addEventListener("click", function(){
      modeButtons[0].classList.remove("selected");
      modeButtons[1].classList.remove("selected");
      this.classList.add("selected");
      this.textContent === "Easy"? numSquares = 3 : numSquares = 6;
      reset();
    });
  }
}

function initSquares(){
  for(var i=0; i < squares.length; i++){
    //add listeners to the squares
    squares[i].addEventListener("click", function(){
      if(this.style.background === colorChose){
      winner.textContent = "Correct!";
      allSquares(colorChose);
      newCols.textContent = "Play Again?";
      }
      else{
        this.style.background = "rgb(5,5,9)";
        winner.textContent = "Try Again!";
      }
    });
   }
}

function reset(){
  newCols.textContent = "New Colors";
  winner.textContent = " ";
  banner.style.background = "rgb(100,180,255)";
    fillArray(numSquares);
    colorChose = colorChosen(numSquares);
  for(var i=0; i < squares.length; i++){
    if(colors[i]){
      squares[i].style.display = "block";
      squares[i].style.background = colors[i];
    } else {
      squares[i].style.display = "none";
    }
  }
}

newCols.addEventListener("click", function(){
  reset();
})


//Function to fill out array with random colors
function fillArray(num){
  if(colors.length > 0){
    while(colors.length > 0){
      colors.pop();
    }
    for(var i = 0 ; i < num; i++){
      colors.push(`rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`);
      }
}
else {
for(var k = 0 ; k < num; k++){
  colors.push(`rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`);
  }
 }
}

//function to make all squares the same color
function allSquares(colorChose){
  banner.style.background = colorChose;
  for(var j=0; j < squares.length; j++){
    squares[j].style.background = colorChose;
   }
}
//function to select the random color
function colorChosen(input){
  var colorChose = colors[`${randNum(input)}`];
  colorDisplay.textContent = colorChose;
  return colorChose;
}
